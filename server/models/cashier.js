const AuthModel = (function() {
	
	function pending(firstname, lastname, username, password, sliitreg, mongoose, cb) {
		const orders = mongoose.model('orders', { 'name': String, 'ordername': String });

		orders.find(function (err, docs) {
      if (err) console.log(err);

      cb(null, docs);
    });
	}

	return {
		pending: pending
	};
})();

module.exports = AuthModel;