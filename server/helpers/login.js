const reg = ['some', 'data', 'within', 'an', 'array'];

//helper function for small tasks
//modular in nature
//revealing modular pattern
const loginHelper = (function() {
	const validateLogin = function(username, password, cb) {
		if(username === undefined || password === undefined) {
			cb('please provide credentials', null);
		}
		else {
			cb(null, true);
		}
	}

	const validateRegister = function(firstname, lastname, username, password, sliitreg, cb) {
		if(firstname === undefined || lastname === undefined || username === undefined || password === undefined || sliitreg === undefined) {
			cb('please provide all info', null);
		}
		else {
			cb(null, true);
		}
	} 

	return {
		validateLogin: validateLogin,
		validateRegister: validateRegister
	}
})();

module.exports = loginHelper;
