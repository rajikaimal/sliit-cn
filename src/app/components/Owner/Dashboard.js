import React from 'react';
import {Bar} from 'react-chartjs-2';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const style = {
    margin: 12,
};
var options = {
    scales: {
        xAxes: [{
            barPercentage: 0.6,
        }],
        yAxes: [{
            ticks: {
                beginAtZero:false
            }
        }]
    }
};

export class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            barData : {
                labels: ['Cashier1', 'Cashier2', 'Cashier3' , 'Cashier4','Cashier5'],
                datasets: [
                    {
                        label:["Total Sellings (Rs.)"],
                        borderWidth: 2,
                        backgroundColor: [
                            '#FF6384',
                            '#10b3e1',
                            '#FFCE56',
                            '#9370db',
                            '#f53e05'
                        ],
                        hoverBackgroundColor: [
                            '#FF6384',
                            '#10b3e1',
                            '#FFCE56',
                            '#9370db',
                            '#f53e05'
                        ],
                        data: [100 ,222, 222 ,60,80]
                    }
                ]
            }
        }
    }
    render(){
        return(
            <div>
                <Bar data={this.state.barData} type="line" height="75" options={options}/>
            </div>
        )
    }
}
